(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.menuToggle = function() {
        var $handle = $('#menu-handle, #menu-close'),
            $menu   = $('#menu-overlay');

        $handle.on('click', function(event) {
            event.preventDefault();
            $menu.toggleClass('open');
        });
    };

    App.bannersHome = function() {
        var $slideshow = $('.banners');
        if (!$slideshow.length) return;

        $slideshow.cycle({
            fx: 'fade',
            slides: '>div'
        });
    };

    App.carouselHome = function() {
        var $wrapper = $('.slick-slider');
        if (!$wrapper.length) return;

        $wrapper.slick({
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow: 5,
            responsive: [
                {
                    breakpoint: 1500,
                    settings: { slidesToShow: 4 }
                },
                {
                    breakpoint: 1080,
                    settings: { slidesToShow: 3 }
                },
                {
                    breakpoint: 768,
                    settings: { slidesToShow: 2 }
                }
            ]
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#response-contato');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val()
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                $response.hide().text('Preencha os campos corretamente.').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.linkExterno = function() {
        var $handle = $('.link-externo');
        if (!$handle.length) return;

        $handle.on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            window.open($(this).data('link'), '_blank');
        });

    };

    App.galeriaPortfolio = function() {
        var $handle  = $('.fancybox'),
            $wrapper = $('.thumb');
        if (!$wrapper.length) return;

        $handle.fancybox({
            padding: 0,
            maxWidth: '80%',
            maxHeight: '90%',
            beforeShow: function() {
                var slug = $(this.element).attr('rel');
                if (slug) {
                    window.location.hash = slug;
                }
            },
            beforeClose: function() {
                window.location.hash = '';
            }
        });

        var hashSlug = window.location.hash;

        if (hashSlug) {
            var el = $('.fancybox[rel=' + hashSlug.substr(1) + ']:eq(0)');

            if (el) el.click();
        }


        $('.thumb').click(function(event) {
            event.preventDefault();

            var slug = $(this).data('slug'),
                el   = $('.fancybox[rel=' + slug + ']:eq(0)');

            if (el) el.click();
        });
    };

    App.init = function() {
        this.menuToggle();
        this.bannersHome();
        this.carouselHome();
        this.linkExterno();
        this.galeriaPortfolio();
        $('#form-contato').on('submit', this.envioContato);
        $('input, textarea').placeholder();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
