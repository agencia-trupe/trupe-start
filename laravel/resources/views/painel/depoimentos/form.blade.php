@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('depoimento', 'Depoimento') !!}
    {!! Form::textarea('depoimento', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<div class="form-group">
    {!! Form::label('cliente', 'Cliente') !!}
    {!! Form::text('cliente', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.depoimentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
