@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Depoimento</h2>
    </legend>

    {!! Form::model($depoimento, [
        'route'  => ['painel.depoimentos.update', $depoimento->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.depoimentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
