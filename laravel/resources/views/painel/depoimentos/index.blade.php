@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Depoimentos
            <a href="{{ route('painel.depoimentos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Depoimento</a>
        </h2>
    </legend>

    @if(!count($depoimentos))
    <div class="alert alert-warning" role="alert">Nenhum depoimento cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="depoimentos">
        <thead>
            <tr>
                <th>Depoimento</th>
                <th>Cliente</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($depoimentos as $depoimento)
                <td>{!! Tools::cropText($depoimento->depoimento, 100) !!}</td>
                <td>{{ $depoimento->cliente }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.depoimentos.destroy', $depoimento->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.depoimentos.edit', $depoimento->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
