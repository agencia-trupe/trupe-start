@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Portfólio
            <div class="btn-group pull-right">
                <a href="{{ route('painel.portfolio.categorias.index') }}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Editar Categorias</a>
                <a href="{{ route('painel.portfolio.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Adicionar Projeto</a>
            </div>
        </h2>
    </legend>

    @if(!count($portfolio))
    <div class="alert alert-warning" role="alert">Nenhum projeto cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="portfolio">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th>Capa</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($portfolio as $projeto)
            <tr class="tr-row" id="{{ $projeto->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $projeto->titulo }}</td>
                <td>
                    <img src="{{ asset('assets/img/portfolio/'.$projeto->imagem_capa) }}" style="width:100%;max-width:100px">
                </td>
                <td><a href="{{ route('painel.portfolio.imagens.index', $projeto->id) }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar</a></td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.portfolio.destroy', $projeto->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.portfolio.edit', $projeto->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
