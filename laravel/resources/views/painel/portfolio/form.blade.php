@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('categorias_list', 'Categorias') !!}
    @if(sizeof($categorias))
        {!! Form::select('categorias_list[]', $categorias, null, ['class' => 'form-control multi-select', 'multiple']) !!}
    @else
        <div class="alert alert-info" role="alert" style="margin:5px 0 0;">Nenhuma categoria cadastrada.</div>
    @endif
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_capa', 'Imagem de Capa (520x350px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/portfolio/'.$portfolio->imagem_capa) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px">
@endif
    {!! Form::file('imagem_capa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_capa_hover', 'Imagem de Capa - Hover (520x350px, opcional)') !!}
@if($submitText == 'Alterar' && $portfolio->imagem_capa_hover)
    <img src="{{ url('assets/img/portfolio/hover/'.$portfolio->imagem_capa_hover) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px">
@endif
    {!! Form::file('imagem_capa_hover', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_home', 'Imagem de Destaque na Home (360x360px, opcional)') !!}
@if($submitText == 'Alterar' && $portfolio->imagem_home)
    <img src="{{ url('assets/img/portfolio/home/'.$portfolio->imagem_home) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px">
@endif
    {!! Form::file('imagem_home', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (opcional)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.portfolio.index') }}" class="btn btn-default btn-voltar">Voltar</a>
