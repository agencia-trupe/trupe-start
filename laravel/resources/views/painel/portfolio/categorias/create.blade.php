@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Portfólio /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.portfolio.categorias.store']) !!}

        @include('painel.portfolio.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
