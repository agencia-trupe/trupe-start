<div class="imagem col-md-2 col-sm-3 col-xs-4" style="margin:5px 0;position:relative;padding:0 5px;" id="{{ $imagem->id }}">
    <img src="{{ url('assets/img/portfolio/imagens/thumbs/'.$imagem->imagem) }}" alt="" style="display:block;width:100%;height:auto;cursor:move;">
    {!! Form::open([
        'route'  => ['painel.portfolio.imagens.destroy', $portfolio->id, $imagem->id],
        'method' => 'delete'
    ]) !!}

    <button type="submit" class="btn btn-danger btn-sm btn-delete" style="position:absolute;bottom:8px;left:10px;"><span class="glyphicon glyphicon-remove"></span></button>

    {!! Form::close() !!}
</div>
