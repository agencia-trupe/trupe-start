<ul class="nav navbar-nav">
    <li @if(str_is('painel.portfolio*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.portfolio.index') }}">Portfólio</a>
    </li>
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.depoimentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.depoimentos.index') }}">Depoimentos</a>
    </li>
    <li @if(str_is('painel.contatos-recebidos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.contatos-recebidos.index') }}">
            Contatos Recebidos
            @if($contatos_nao_lidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatos_nao_lidos }}</span>
            @endif
        </a>
    </li>
</ul>
