@extends('frontend.common.template')

@section('content')

    <section class="main diferenciais">
        <div class="center">
            <h2 class="title">Diferenciais</h2>

            <div class="diferenciais-item">
                <div class="diferenciais-item-texto">
                    <h3>Layout personalizado</h3>
                    <p>Nossa equipe de designers cria um layout exclusivo para o seu projeto, tudo é personalizado!</p>
                </div>
                <div class="diferenciais-item-imagem">
                    <img src="{{ asset('assets/img/layout/diferenciais1.png') }}" alt="">
                </div>
            </div>
            <div class="diferenciais-item">
                <div class="diferenciais-item-texto">
                    <h3>Não usamos templates</h3>
                    <p>Nossos websites são personalizados, não usamos templates ou Wordpress no desenvolvimento, entendemos que cada empresa tem suas necessidades, e merece um design único.</p>
                </div>
                <div class="diferenciais-item-imagem">
                    <img src="{{ asset('assets/img/layout/diferenciais2.png') }}" alt="">
                </div>
            </div>
            <div class="diferenciais-item">
                <div class="diferenciais-item-texto">
                    <h3>Site otimizado para melhor rankeamento em sites de busca</h3>
                    <p>Nossos websites são desenvolvidos para que sua empresa apareça no Google, todos os projetos são desenvolvidos de maneira correta, e tecnicamente bem construídos, fazendo com que sua empresa aparece para milhões de pessoas através dos mecanismos de busca.</p>
                </div>
                <div class="diferenciais-item-imagem">
                    <img src="{{ asset('assets/img/layout/diferenciais3.png') }}" alt="">
                </div>
            </div>
            <div class="diferenciais-item">
                <div class="diferenciais-item-texto">
                    <h3>CMS - Sistema de gerenciamento de conteúdo próprio</h3>
                    <p>Nosso gerenciador de conteúdo para atualizações no site é próprio, muito mais fácil de utilizar que o Wordpress, dessa forma fica muito mais simples inserir ou editar informações no site.</p>
                </div>
                <div class="diferenciais-item-imagem">
                    <img src="{{ asset('assets/img/layout/diferenciais4.png') }}" alt="">
                </div>
            </div>
            <div class="diferenciais-item">
                <div class="diferenciais-item-texto">
                    <h3>Sem custo de mensalidade</h3>
                    <p>Não cobramos mensalidade, você só paga o desenvolvimento e pronto!</p>
                </div>
                <div class="diferenciais-item-imagem">
                    <img src="{{ asset('assets/img/layout/diferenciais5.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

@endsection
