@extends('frontend.common.template')

@section('content')

    <section class="main contato">
        <div class="center">
            <h2 class="title">Contato</h2>
        </div>
        @include('frontend.contato_form')
    </section>

@endsection
