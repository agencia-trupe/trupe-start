    <header @if(Route::currentRouteName() === 'home') class="home" @endif>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ config('site.name') }}</a></h1>
            <button type="button" id="menu-handle">Menu</button>
        </div>
    </header>
    <div id="menu-overlay">
        <nav>
            <a href="#" id="menu-close">close</a>
            <a href="{{ route('home') }}" @if(Route::currentRouteName() === 'home') class="active" @endif>Home</a>
            <a href="{{ route('agencia') }}" @if(Route::currentRouteName() === 'agencia') class="active" @endif>Agência</a>
            <a href="{{ route('portfolio') }}" @if(str_is('portfolio*', Route::currentRouteName())) class="active" @endif>Portfólio</a>
            <a href="{{ route('diferenciais') }}" @if(Route::currentRouteName() === 'diferenciais') class="active" @endif>Diferenciais</a>
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif>Contato</a>
        </nav>
    </div>
