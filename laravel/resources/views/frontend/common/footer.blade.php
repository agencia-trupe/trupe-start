    <footer>
        <div class="center">
            <div class="left">
                <p class="endereco">
                    Rua Aureliano Guimarães, 150 - sala 522<br>
                    Vila Andrade - 05727-160 - São Paulo - SP
                </p>
            </div>

            <div class="right">
                <p class="chamada">E então, vamos trabalhar juntos?</p>
                <p class="informacoes">
                    (011) 2861.1590
                    <a href="https://www.facebook.com/trupestart" target="_blank" class="facebook">facebook</a>
                    <a href="{{ route('contato') }}" class="contato">contato</a>
                </p>
            </div>
        </div>
    </footer>
