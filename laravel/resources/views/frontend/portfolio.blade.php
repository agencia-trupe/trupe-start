@extends('frontend.common.template')

@section('content')

    <section class="main portfolio">
        <div class="center">
            <h2 class="title">Portfólio</h2>

            <nav class="categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('portfolio.categoria', $cat->slug) }}" @if($cat == $categoria) class="active" @endif>{{ $cat->titulo }}</a>
                @endforeach
            </nav>

            <div class="projetos">
                @foreach($portfolio as $projeto)
                <a href="#" class="thumb" data-slug="{{ $projeto->slug }}">
                    <img src="{{ asset('assets/img/portfolio/'.$projeto->imagem_capa) }}">
                    @if($projeto->imagem_capa_hover)
                    <div class="imagem-hover">
                        <img src="{{ asset('assets/img/portfolio/hover/'.$projeto->imagem_capa_hover) }}">
                    </div>
                    @endif
                    @if($projeto->link)
                    <div class="overlay">
                        <span data-link="{!! Tools::formatLink($projeto->link) !!}" class="link-externo"></span>
                        <span class="ver-mais"></span>
                    </div>
                    @endif
                </a>
                @endforeach
            </div>
        </div>
    </section>

    <div class="hidden">
    @foreach($portfolio as $projeto)
        @foreach($projeto->imagens as $imagem)
            <a href="{{ asset('assets/img/portfolio/imagens/'.$imagem->imagem) }}" class="fancybox" rel="{{ $projeto->slug }}"></a>
        @endforeach
    @endforeach
    </div>

@endsection