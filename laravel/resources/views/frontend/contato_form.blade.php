<div class="center">
    <div class="left">
        <h3>E então, vamos <span>trabalhar juntos?</span></h3>
        <p>Mande uma mensagem. Aceitamos solicitacões de orçamento, críticas, sugestões, dúvidas e até elogios!</p>
        <p class="informacoes">
            (011) 2861.1590
            <a href="https://www.facebook.com/trupestart" target="_blank" class="facebook">facebook</a>
        </p>
        <p>
            Rua Aureliano Guimarães, 150 - sala 522 - Vila Andrade<br>
            05727-160 - São Paulo - SP
        </p>
    </div>

    <form action="{{ route('contato.envio') }}" method="POST" id="form-contato">
        {!! csrf_field() !!}
        <input type="text" name="nome" id="nome" placeholder="NOME" required>
        <input type="email" name="email" id="email" placeholder="E-MAIL" required>
        <input type="text" name="telefone" id="telefone" placeholder="TELEFONE">
        <textarea name="mensagem" id="mensagem" placeholder="MENSAGEM" required></textarea>
        <input type="submit" value="Enviar">
        <div class="response-contato-wrapper">
            <div id="response-contato"></div>
        </div>
    </form>
</div>

<div class="googlemaps">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.3376929037745!2d-46.73742430000001!3d-23.628074499999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce514e3c573059%3A0x1816c8c11a9faac9!2sR.+%C3%81ureliano+Guimar%C3%A3es%2C+150+-+Vila+Andrade%2C+S%C3%A3o+Paulo+-+SP%2C+05727-160!5e0!3m2!1spt-BR!2sbr!4v1443811464634" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>