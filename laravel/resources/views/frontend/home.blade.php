@extends('frontend.common.template')

@section('content')

    <section class="home-banners">
        <div class="center">
            <div class="chamada">
                <h1>Estratégia<br><span>+</span> Design</h1>
                <h2>Adoramos fazer peças interativas inteligentes e simples, que são <span>boas de ver</span> e <span>ótimas pra usar</span>.</h2>
                <a href="{{ route('agencia') }}">Agência</a>
                <a href="{{ route('portfolio') }}">Portfólio</a>
            </div>
        </div>
        <div class="banners">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}');"></div>
            @endforeach
        </div>
    </section>

    <section class="home-servicos">
        <div class="center">
            <h3>Serviços Profissionais de Comunicação para o seu Negócio</h3>
            <nav class="categorias">
                @foreach($categorias as $categoria)
                <a href="{{ route('portfolio.categoria', $categoria->slug) }}">{{ $categoria->titulo }}</a>
                @endforeach
            </nav>
            <div class="icones">
                <a href="{{ route('diferenciais') }}">
                    <div class="icone icone-1"></div>
                    Acerte na <br>comunicação
                </a>
                <a href="{{ route('diferenciais') }}">
                    <div class="icone icone-2"></div>
                    Custos <br>reduzidos
                </a>
                <a href="{{ route('diferenciais') }}">
                    <div class="icone icone-3"></div>
                    Prazo e <br>agilidade
                </a>
                <a href="{{ route('diferenciais') }}">
                    <div class="icone icone-4"></div>
                    Mais clientes <br>para o seu negócio
                </a>
            </div>
        </div>
    </section>

    <section class="home-numeros">
        <div class="center">
            <div class="col">
                <span class="numero">+300</span>
                <span class="texto">Sites <br>online</span>
            </div>
            <div class="col">
                <span class="numero">+200</span>
                <span class="texto">Projetos <br>impressos</span>
            </div>
            <div class="col">
                <span class="numero">+350</span>
                <span class="texto">Clientes <br>satisfeitos</span>
            </div>
            <div class="col">
                <span class="numero">+30</span>
                <span class="texto">Cafézinhos <br>por dia</span>
            </div>
        </div>
    </section>

    @if(count($portfolio))
    <section class="home-portfolio">
        <div class="slick-slider">
            @foreach($portfolio as $projeto)
            <a href="{{ route('portfolio') }}#{{ $projeto->slug }}" class="slide">
                <img src="{{ asset('assets/img/portfolio/home/'.$projeto->imagem_home) }}">
                <div class="overlay">
                    <div class="titulo">
                        <span>{{ $projeto->titulo }}</span>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </section>
    @endif

    @if(count($depoimento))
    <section class="home-depoimento">
        <div class="center">
            <div class="depoimento">
                {!! $depoimento->depoimento !!}
                <p class="cliente">{{ $depoimento->cliente }}</p>
            </div>
        </div>
    </section>
    @endif

    <section class="contato home-contato">
        @include('frontend.contato_form')
    </section>

@endsection
