@extends('frontend.common.template')

@section('content')

    <section class="main agencia">
        <div class="center">
            <h2 class="title">Agência</h2>

            <div class="agencia-texto">
                <div class="left">
                    <p>A <strong>Trupe·Start</strong> é uma divisão da <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>. Empresa com mais de 10 anos de experiência no mercado de comunicação corporativa, criada para atender negócios ou ideias em formação.</p>
                    <p>A Trupe percebeu que pequenas empresas e profissionais liberais encontram muita dificuldade no acesso a serviços profissionais de branding e comunicação. Muitas vezes porque o valor cobrado por grandes agências está fora do budget do negócio, e outras vezes porque falta conhecimento sobre o que realmente precisam para melhorar a comunicação e vender mais.</p>
                </div>
                <div class="right">
                    <img src="{{ asset('assets/img/layout/logo-trupe.png') }}" alt="">
                    <p class="destaque">A Trupe·Start coloca sua empresa em evidência!</p>
                </div>
            </div>

            <div class="agencia-pessoas">
                <div>
                    <img src="{{ asset('assets/img/layout/agencia/fernando.jpg') }}" alt="">
                    <div class="overlay">
                        <span class="nome">Fernando Rocha</span>
                        <span class="descricao">Novos negócios</span>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/agencia/leila.jpg') }}" alt="">
                    <div class="overlay">
                        <span class="nome">Leila Arruda</span>
                        <span class="descricao">Diretora de criação</span>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/agencia/rafael.jpg') }}" alt="">
                    <div class="overlay">
                        <span class="nome">Rafael Ruiz</span>
                        <span class="descricao">Designer</span>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/agencia/bruno.jpg') }}" alt="">
                    <div class="overlay">
                        <span class="nome">Bruno Monteiro</span>
                        <span class="descricao">Desenvolvedor PHP</span>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/agencia/patrick.jpg') }}" alt="">
                    <div class="overlay">
                        <span class="nome">Patrick Marcelo</span>
                        <span class="descricao">Desenvolvedor PHP</span>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/agencia/luiz.jpg') }}" alt="">
                    <div class="overlay">
                        <span class="nome">Luiz Batanero</span>
                        <span class="descricao">Desenvolvedor PHP</span>
                    </div>
                </div>
                <div class="frank">
                    <img src="{{ asset('assets/img/layout/agencia/frank.jpg') }}" alt="">
                    <div class="overlay">
                        <span class="nome">Frank</span>
                        <span class="descricao">Agente anti-estresse</span>
                    </div>
                </div>
            </div>

            <a href="{{ route('contato') }}" class="agencia-orcamento">
                <span>Solicite um orçamento</span>
            </a>
        </div>
    </section>

@endsection
