<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->string('imagem_capa');
            $table->string('imagem_capa_hover');
            $table->string('imagem_home');
            $table->string('link');
            $table->timestamps();
        });

        Schema::create('portfolio_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('portfolio_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('portfolio_projeto_categoria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_id')->unsigned()->index();
            $table->integer('categoria_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('portfolio_projeto_categoria', function (Blueprint $table) {
            $table->foreign('portfolio_id')->references('id')->on('portfolio')->onDelete('cascade');
            $table->foreign('categoria_id')->references('id')->on('portfolio_categorias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_projeto_categoria');
        Schema::drop('portfolio_imagens');
        Schema::drop('portfolio_categorias');
        Schema::drop('portfolio');
    }
}
