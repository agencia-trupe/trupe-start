<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PortfolioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'categorias_list'   => 'required',
            'titulo'            => 'required',
            'imagem_capa'       => 'required|image',
            'imagem_capa_hover' => 'image',
            'imagem_home'       => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_capa'] = 'image';
            $rules['imagem_home'] = 'image';
        }

        return $rules;
    }
}
