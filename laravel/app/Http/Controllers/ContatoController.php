<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContatoRecebidoRequest;
use App\Models\ContatoRecebido;
use Illuminate\Http\Request;

class ContatoController extends Controller
{
    public function index()
    {
        return view('frontend.contato');
    }

    public function envio(ContatoRecebidoRequest $request)
    {
        ContatoRecebido::create($request->all());

        \Mail::send('emails.contato', $request->all(), function($message) use ($request) {
            $message->to('contato@trupe.net', config('site.name'))
                    ->subject('[CONTATO] '.config('site.name'))
                    ->replyTo($request->get('email'), $request->get('nome'));
        });

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
