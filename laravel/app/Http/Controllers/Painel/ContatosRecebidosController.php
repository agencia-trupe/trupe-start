<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Models\ContatoRecebido;
use App\Http\Controllers\Controller;

class ContatosRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ContatoRecebido::orderBy('id', 'DESC')->paginate(15);

        return view('painel.contatosrecebidos.index', compact('contatosrecebidos'));
    }

    public function show(ContatoRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contatosrecebidos.show', compact('contato'));
    }

    public function destroy(ContatoRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contatos-recebidos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }
}
