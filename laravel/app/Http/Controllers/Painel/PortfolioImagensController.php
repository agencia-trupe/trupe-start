<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PortfolioImagensRequest;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use App\Models\PortfolioImagem;

use App\Helpers\CropImage;

class PortfolioImagensController extends Controller
{
    private $image_config = [
        [
            'width'  => 980,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/portfolio/imagens/'
        ],
        [
            'width'  => 150,
            'height' => 150,
            'path'   => 'assets/img/portfolio/imagens/thumbs/'
        ]
    ];

    public function index(Portfolio $portfolio)
    {
        $imagens = PortfolioImagem::portfolio($portfolio->id)->ordenados()->get();

        return view('painel.portfolio.imagens.index', compact('imagens', 'portfolio'));
    }

    public function create(Portfolio $portfolio)
    {
        return view('painel.portfolio.imagens.create', compact('portfolio'));
    }

    public function store(Portfolio $portfolio, PortfolioImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = $portfolio->imagens()->create($input);
            $view = view('painel.portfolio.imagens.imagem', compact('portfolio', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Portfolio $portfolio, PortfolioImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.portfolio.imagens.index', $portfolio)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
