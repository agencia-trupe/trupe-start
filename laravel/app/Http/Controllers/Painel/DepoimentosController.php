<?php

namespace App\Http\Controllers\Painel;

use App\Models\Depoimento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DepoimentosRequest;

class DepoimentosController extends Controller
{
    public function index()
    {
        $depoimentos = Depoimento::orderBy('id', 'DESC')->get();

        return view('painel.depoimentos.index', compact('depoimentos'));
    }

    public function create()
    {
        return view('painel.depoimentos.create');
    }

    public function store(DepoimentosRequest $request)
    {
        try {

            Depoimento::create($request->all());
            return redirect()->route('painel.depoimentos.index')->with('success', 'Depoimento adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar depoimento: '.$e->getMessage()]);

        }
    }

    public function edit(Depoimento $depoimento)
    {
        return view('painel.depoimentos.edit', compact('depoimento'));
    }

    public function update(DepoimentosRequest $request, Depoimento $depoimento)
    {
        try {

            $depoimento->update($request->all());
            return redirect()->route('painel.depoimentos.index')->with('success', 'Depoimento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar depoimento: '.$e->getMessage()]);

        }
    }

    public function destroy(Depoimento $depoimento)
    {
        try {

            $depoimento->delete();
            return redirect()->route('painel.depoimentos.index')->with('success', 'Depoimento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir depoimento: '.$e->getMessage()]);

        }
    }

}
