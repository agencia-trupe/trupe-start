<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\PortfolioCategoriasRequest;
use App\Models\PortfolioCategoria;
use Illuminate\Http\Request;

class PortfolioCategoriasController extends Controller
{
    public function index()
    {
        $categorias = PortfolioCategoria::ordenados()->get();

        return view('painel.portfolio.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.portfolio.categorias.create');
    }

    public function store(PortfolioCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            PortfolioCategoria::create($input);
            return redirect()->route('painel.portfolio.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(PortfolioCategoria $categoria)
    {
        return view('painel.portfolio.categorias.edit', compact('categoria'));
    }

    public function update(PortfolioCategoriasRequest $request, PortfolioCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.portfolio.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(PortfolioCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.portfolio.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
