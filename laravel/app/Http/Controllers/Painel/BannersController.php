<?php

namespace App\Http\Controllers\Painel;

use App\Models\Banner;
use App\Helpers\CropImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannersRequest;

class BannersController extends Controller
{
    private $image_config = [
        'width'  => null,
        'height' => 450,
        'upsize' => true,
        'path'   => 'assets/img/banners/'
    ];

    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('painel.banners.index', compact('banners'));
    }

    public function create()
    {
        return view('painel.banners.create');
    }

    public function store(BannersRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Banner::create($input);
            return redirect()->route('painel.banners.index')->with('success', 'Banner adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar banner: '.$e->getMessage()]);

        }
    }

    public function edit(Banner $banner)
    {
        return view('painel.banners.edit', compact('banner'));
    }

    public function update(BannersRequest $request, Banner $banner)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $banner->update($input);
            return redirect()->route('painel.banners.index')->with('success', 'Banner alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar banner: '.$e->getMessage()]);

        }
    }

    public function destroy(Banner $banner)
    {
        try {

            $banner->delete();
            return redirect()->route('painel.banners.index')->with('success', 'Banner excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir banner: '.$e->getMessage()]);

        }
    }
}
