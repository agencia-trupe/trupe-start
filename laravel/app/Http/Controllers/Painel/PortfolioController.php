<?php

namespace App\Http\Controllers\Painel;

use App\Helpers\CropImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\PortfolioRequest;
use App\Models\Portfolio;
use App\Models\PortfolioCategoria;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    private $image_config = [
        'imagem_capa' => [
            'width'  => 520,
            'height' => 350,
            'path'   => 'assets/img/portfolio/'
        ],
        'imagem_capa_hover' => [
            'width'  => 520,
            'height' => 350,
            'path'   => 'assets/img/portfolio/hover/'
        ],
        'imagem_home' => [
            'width'  => 360,
            'height' => 360,
            'path'   => 'assets/img/portfolio/home/'
        ],
    ];

    private $categorias;

    public function __construct()
    {
        $this->categorias = PortfolioCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $portfolio = Portfolio::ordenados()->get();

        return view('painel.portfolio.index', compact('categorias', 'portfolio'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.portfolio.create', compact('categorias'));
    }

    public function store(PortfolioRequest $request)
    {
        try {

            $input = $request->except('categorias_list');

            $input['imagem_capa'] = CropImage::make('imagem_capa', $this->image_config['imagem_capa']);
            $input['imagem_home'] = CropImage::make('imagem_home', $this->image_config['imagem_home']);

            if (isset($input['imagem_capa_hover'])) {
                $input['imagem_capa_hover'] = CropImage::make('imagem_capa_hover', $this->image_config['imagem_capa_hover']);
            }

            $portfolio = Portfolio::create($input);
            $portfolio->categorias()->sync($request->input('categorias_list'));

            return redirect()->route('painel.portfolio.index')->with('success', 'Projeto adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar projeto: '.$e->getMessage()]);

        }
    }

    public function edit(Portfolio $portfolio)
    {
        $categorias = $this->categorias;

        return view('painel.portfolio.edit', compact('categorias', 'portfolio'));
    }

    public function update(PortfolioRequest $request, Portfolio $portfolio)
    {
        try {

            $input = array_filter($request->except('categorias_list'), 'strlen');

            if (isset($input['imagem_capa'])) {
                $input['imagem_capa'] = CropImage::make('imagem_capa', $this->image_config['imagem_capa']);
            }
            if (isset($input['imagem_home'])) {
                $input['imagem_home'] = CropImage::make('imagem_home', $this->image_config['imagem_home']);
            }
            if (isset($input['imagem_capa_hover'])) {
                $input['imagem_capa_hover'] = CropImage::make('imagem_capa_hover', $this->image_config['imagem_capa_hover']);
            }

            $portfolio->update($input);
            $portfolio->categorias()->sync($request->input('categorias_list'));

            return redirect()->route('painel.portfolio.index')->with('success', 'Projeto alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar projeto: '.$e->getMessage()]);

        }
    }

    public function destroy(Portfolio $portfolio)
    {
        try {

            $portfolio->delete();
            return redirect()->route('painel.portfolio.index')->with('success', 'Projeto excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir projeto: '.$e->getMessage()]);

        }
    }
}
