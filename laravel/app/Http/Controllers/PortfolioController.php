<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Portfolio;
use App\Models\PortfolioCategoria;
use App\Http\Controllers\Controller;

class PortfolioController extends Controller
{
    public function index()
    {
        $portfolio = Portfolio::with('imagens')->ordenados()->get();
        $categoria = null;

        $categorias = PortfolioCategoria::ordenados()->get();

        return view('frontend.portfolio', compact('portfolio', 'categoria', 'categorias'));
    }

    public function categoria(PortfolioCategoria $categoria)
    {
        $portfolio = $categoria->projetos()->with('imagens')->ordenados()->get();

        $categorias = PortfolioCategoria::ordenados()->get();

        return view('frontend.portfolio', compact('portfolio', 'categoria', 'categorias'));
    }
}
