<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Banner;
use App\Models\Depoimento;
use App\Models\Portfolio;
use App\Models\PortfolioCategoria;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $banners    = Banner::ordenados()->get();
        $depoimento = Depoimento::orderByRaw("RAND()")->first();
        $categorias = PortfolioCategoria::ordenados()->get();
        $portfolio  = Portfolio::where('imagem_home', '!=', false)->orderByRaw("RAND()")->limit(10)->select('slug', 'titulo', 'imagem_home')->get();

        return view('frontend.home', compact('banners', 'depoimento', 'categorias', 'portfolio'));
    }
}
