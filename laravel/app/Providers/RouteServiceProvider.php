<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('portfolio', 'App\Models\Portfolio');
        $router->model('categorias', 'App\Models\PortfolioCategoria');
        $router->model('imagens', 'App\Models\PortfolioImagem');
        $router->model('banners', 'App\Models\Banner');
        $router->model('depoimentos', 'App\Models\Depoimento');
        $router->model('contatos-recebidos', 'App\Models\ContatoRecebido');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('categoria_slug', function($value) {
            return \App\Models\PortfolioCategoria::slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
