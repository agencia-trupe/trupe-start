<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Portfolio extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true,
    ];

    protected $table = 'portfolio';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function categorias()
    {
        return $this->belongsToMany('App\Models\PortfolioCategoria', 'portfolio_projeto_categoria', 'portfolio_id', 'categoria_id')->orderBy('ordem', 'ASC');
    }

    public function getCategoriasListAttribute()
    {
        return $this->categorias->lists('id')->toArray();
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\PortfolioImagem', 'portfolio_id')
            ->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
