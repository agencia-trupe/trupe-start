<?php

namespace App\Helpers;

class Tools
{

    public static function cropText($text = null, $length = null)
    {
        if (!$text || !$length) return false;

        $length = abs((int)$length);
        $text   = strip_tags($text);

        if(strlen($text) > $length) {
            $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
        }

        return $text;
    }

    public static function formatLink($link)
    {
        if ($link == 'http://' OR $link == '') return '';

        $url = parse_url($link);

        if (!$url OR !isset($url['scheme']))
            $link = 'http://'.$link;

        return $link;
    }

}
