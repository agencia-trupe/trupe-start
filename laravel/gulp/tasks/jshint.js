var config  = require('../config'),
    gulp    = require('gulp'),
    jshint  = require('gulp-jshint'),
    plumber = require('gulp-plumber');

gulp.task('jsHint', function() {
    return gulp.src(config.development.js + '**/*.js')
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
});
